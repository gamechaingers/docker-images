#!/bin/bash
set -ex
export DEBIAN_FRONTEND=noninteractive

curl -sLf --retry 3 --tlsv1.2 --proto "=https" 'https://packages.doppler.com/public/cli/gpg.DE2A7741A397C129.key' | sudo apt-key add -
echo "deb https://packages.doppler.com/public/cli/deb/debian any-version main" | sudo tee /etc/apt/sources.list.d/doppler-cli.list
apt-get update
apt-get install -y apt-transport-https ca-certificates curl doppler gnupg git python3-pip redis-server gettext fish npm ruby-dev python3.9-venv python3.9-dev
npm install -g log4brains sass

sudo ln -s /usr/bin/python3 /usr/bin/python
