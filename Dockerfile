FROM gitpod/workspace-full:latest

COPY requirements.txt /app/requirements.txt

WORKDIR /app

USER root
RUN pip install --no-cache-dir  -U -r /app/requirements.txt
USER gitpod
